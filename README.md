Golang JWT Example

## API Routes
#### POST 127.0.0.1:8080/user/login

##### Send
Body:
{
	"username": "johnsmith"
}

##### Returns
{
  "jwt_token": "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VybmFtZSI6ImpvaG5zbWl0aCIsImV4cCI6MTU5ODI1NTE3MSwiaXNzIjoiTW9tZW50aXVtT3JnMSJ9.GeQKt5GcRAjCwttI1y3Zr97DeSvEqr4l5Q4dAX4dgV0"
}

#### GET 127.0.0.1:8080/user/happy
##### Send
Header: Make sure a given Bearer token is in the header.

Body: nil.

##### Returns
Very happy :)