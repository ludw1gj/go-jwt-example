package main

import (
	"encoding/json"
	"errors"
	"github.com/gorilla/mux"
	"io/ioutil"
	"log"
	"main/jwtHandler"
	"net/http"
	"strings"
)

var usernamesInDb = []string{"johnsmith"}
var signingKey = []byte("secretKey")
var issuer = "MomentiumOrg1"

type User struct {
	Username string `json:"username"`
}

type LogInResponse struct {
	JWTToken string `json:"jwt_token"`
}


func main() {
	router := createRouter()
	http.Handle("/", router)
	log.Fatal(http.ListenAndServe(":8080", nil))
}

func createRouter() *mux.Router {
	r := mux.NewRouter()
	r.HandleFunc("/user/login", LogInHandler).Methods("POST")
	r.HandleFunc("/user/happy", HappyHandler).Methods("GET")
	return r
}

func LogInHandler(w http.ResponseWriter, r *http.Request) {
	defer r.Body.Close()

	// parse given user data
	var user User
	body, err := ioutil.ReadAll(r.Body)
	if err != nil {
		panic(err.Error())
	}
	if err := json.Unmarshal(body, &user); err != nil {
		w.WriteHeader(http.StatusBadRequest)
		_, _ = w.Write([]byte("could not unmarshal"))
		return
	}

	// find user exists in 'db'
	userExists := false
	for _, username := range usernamesInDb {
		if username == user.Username {
			userExists = true
		}
	}
	if !userExists {
		w.WriteHeader(http.StatusForbidden)
		_, _ = w.Write([]byte("username doesn't exist"))
		return
	}

	// create token for user
	signedToken, err := createToken(user.Username)
	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
		_, _ = w.Write([]byte("could not create token"))
		return
	}

	logInResp := LogInResponse{
		JWTToken: signedToken,
	}
	result, err := json.Marshal(logInResp)
	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
		_, _ = w.Write([]byte("could not create token"))
		return
	}

	// send token to user
	w.WriteHeader(http.StatusOK)
	_, _ = w.Write(result)
	usernamesInDb = append(usernamesInDb, user.Username)
}

func HappyHandler(w http.ResponseWriter, r *http.Request) {
	defer r.Body.Close()

	// extract and validate token
	extractedToken := r.Header.Get("Authorization")
	signedToken := strings.Split(extractedToken, "Bearer ")[1]
	if ok := validateToken(signedToken); !ok {
		w.WriteHeader(http.StatusForbidden)
		_, _ = w.Write([]byte("unauthorised :("))
		return
	}
	w.WriteHeader(http.StatusOK)
	_, _ = w.Write([]byte("Very happy :)"))
}

func validateToken(signedToken string) bool {
	parsedClaims, err := jwtHandler.ParseClaims(signedToken, signingKey)
	if err != nil {
		return false
	}
	err = jwtHandler.ValidateClaim(parsedClaims)
	if err != nil {
		return false
	}
	for _, username := range usernamesInDb {
		if username == parsedClaims.Username {
			return true
		}
	}
	return false
}

func createToken(username string) (string, error) {
	claims := jwtHandler.BuildClaims(username, issuer)
	signedToken, err := jwtHandler.GenerateSignedToken(claims, signingKey)
	if err != nil {
		return "", errors.New("could not generate token")
	}
	return signedToken, nil
}