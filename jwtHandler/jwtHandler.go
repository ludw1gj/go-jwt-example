package jwtHandler

import (
	"errors"
	"github.com/dgrijalva/jwt-go"
	"time"
)

type CustomClaims struct {
	Username string `json:"username"`
	jwt.StandardClaims
}

func BuildClaims(username string, issuer string) CustomClaims {
	thirtyDaysInSeconds := int64(2592000)
	currentUnixTimeInSeconds := time.Now().UTC().Unix()
	expiry := currentUnixTimeInSeconds + thirtyDaysInSeconds
	return CustomClaims{
		Username: username,
		StandardClaims: jwt.StandardClaims{
			ExpiresAt: expiry,
			Issuer: issuer,
		},
	}
}

func GenerateSignedToken(claims CustomClaims, signingKey []byte) (string, error) {
	token := jwt.NewWithClaims(jwt.SigningMethodHS256, claims)
	signedToken, err := token.SignedString(signingKey)
	if err != nil {
		return "", errors.New("could not sign token")
	}
	return signedToken, nil
}

func ParseClaims(signedToken string, signingKey []byte) (*CustomClaims, error) {
	// parse signed token
	parsedToken, err := jwt.ParseWithClaims(
		signedToken,
		&CustomClaims{},
		func(token *jwt.Token) (interface{}, error) {
			return signingKey, nil
		},
	)
	if err != nil {
		return nil, errors.New("could not parse signed token")
	}

	// parse claim
	parsedClaims, ok := parsedToken.Claims.(*CustomClaims)
	if !ok {
		return nil, errors.New("couldn't parse claims")
	}
	return parsedClaims, nil
}

func ValidateClaim(claims *CustomClaims) error {
	if invalid := claims.Valid(); invalid != nil {
		return errors.New("JWT is expired")
	}
	return nil
}

